part of 'shared.dart';

class SharedWidget {
  static notifFlushbar(int duration, String massage) {
    return Flushbar(
      duration: Duration(milliseconds: duration),
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: Color(0xFFFF5c8e),
      message: massage,
    );
  }

  static spinKitCircle() {
    return SpinKitFadingCircle(
      color: accentColor2,
      size: 50,
    );
  }
}
