part of 'pages.dart';

// ignore: must_be_immutable
class SelectSeatPage extends StatefulWidget {
  Ticket ticket;

  SelectSeatPage(this.ticket);

  @override
  _SelectSeatPageState createState() => _SelectSeatPageState();
}

class _SelectSeatPageState extends State<SelectSeatPage> {
  List<String> selectedSeats = [];
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context
            .bloc<PageBloc>()
            .add(GoToSelectSchedulePage(widget.ticket.movieDetail));
        return;
      },
      child: Scaffold(
          body: Stack(children: <Widget>[
        Container(
          color: accentColor1,
        ),
        SafeArea(
            child: Container(
          color: Colors.white,
        )),
        ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 30, left: defaultMargin),
                      padding: EdgeInsets.all(1),
                      child: GestureDetector(
                        onTap: () {
                          context.bloc<PageBloc>().add(GoToSelectSchedulePage(
                              widget.ticket.movieDetail));
                        },
                        child: Icon(
                          Icons.arrow_back,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20, right: defaultMargin),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.5,
                            margin: EdgeInsets.only(right: 16),
                            child: Text(
                              widget.ticket.movieDetail.title,
                              style: blackTextFont.copyWith(fontSize: 20),
                              maxLines: 2,
                              overflow: TextOverflow.clip,
                              textAlign: TextAlign.end,
                            ),
                          ),
                          // Text(widget.ticket.movieDetail.posterPath),
                          Container(
                            height: 60,
                            width: 60,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                image: DecorationImage(
                                    image: NetworkImage(imageBaseURL +
                                        "w154" +
                                        widget.ticket.movieDetail.posterPath),
                                    fit: BoxFit.cover)),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                //Sreen Cenima
                Container(
                  margin: EdgeInsets.only(top: 30),
                  width: 277,
                  height: 84,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/screen.png"))),
                ),
                //Seats
                generateSeat(),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.topCenter,
                  child: BlocBuilder<UserBloc, UserState>(
                    builder: (_, userState) => FloatingActionButton(
                      elevation: 0,
                      backgroundColor:
                          (selectedSeats.length > 0) ? mainColor : Colors.grey,
                      child: Icon(
                        Icons.arrow_forward,
                        color: (selectedSeats.length > 0)
                            ? Colors.white
                            : Colors.black,
                      ),
                      onPressed: selectedSeats.length > 0
                          ? () {
                              context
                                  .bloc<PageBloc>()
                                  .add(GoToCheckoutPage(widget.ticket));
                            }
                          : null,
                    ),
                  ),
                ),
                SizedBox(height: 30),
              ],
            )
          ],
        )
      ])),
    );
  }

  Column generateSeat() {
    List<int> numberofSeats = [3, 5, 5, 5, 5];
    List<Widget> widgets = [];
    for (var i = 0; i < numberofSeats.length; i++) {
      widgets.add(
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(
            numberofSeats[i],
            (index) => Padding(
              padding: EdgeInsets.only(
                  right: index < numberofSeats[i] - 1 ? 16 : 0, bottom: 16),
              child: SelectableBox(
                "${String.fromCharCode(i + 65)} ${index + 1}",
                width: 45,
                height: 45,
                textStyle: whiteNumberFont.copyWith(color: Colors.black),
                isSelected: selectedSeats
                    .contains("${String.fromCharCode(i + 65)}${index + 1}"),
                isEnabled: index != 0,
                onTap: () {
                  String seatNumber =
                      "${String.fromCharCode(i + 65)}${index + 1}";
                  setState(() {
                    if (selectedSeats.contains(seatNumber)) {
                      selectedSeats.remove(seatNumber);
                    } else {
                      selectedSeats.add(seatNumber);
                    }
                  });
                },
              ),
            ),
          ),
        ),
      );
    }
    return Column(children: widgets);
  }
}
