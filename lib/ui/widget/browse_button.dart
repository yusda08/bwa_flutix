part of 'widgets.dart';

class BrowseButton extends StatelessWidget {
  final String genre;
  BrowseButton(this.genre);

  @override
  Widget build(BuildContext context) {
    double width =
        (MediaQuery.of(context).size.width - 4 * defaultMargin - 24) / 4;
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            width: width,
            height: 68,
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(bottom: 5),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: Color(0xffE5E5E5)),
            child: Image(
                image: AssetImage((genre == 'Horror')
                    ? "assets/ic_horror.png"
                    : (genre == 'Music')
                        ? "assets/ic_music.png"
                        : (genre == 'Action')
                            ? "assets/ic_action.png"
                            : (genre == 'Drama')
                                ? "assets/ic_drama.png"
                                : (genre == 'War')
                                    ? "assets/ic_war.png"
                                    : "assets/ic_crime.png")),
          ),
          Text(
            genre,
            style: blackTextFont.copyWith(
                fontSize: 14, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}
