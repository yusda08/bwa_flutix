part of 'widgets.dart';

class RatingStar extends StatelessWidget {
  final double voteAvarage;
  final double starSize;
  final double fontSize;
  final Color color;
  final MainAxisAlignment alignment;
  const RatingStar(
      {this.voteAvarage = 0,
      this.starSize = 20,
      this.fontSize = 12,
      this.color,
      this.alignment = MainAxisAlignment.start});

  @override
  Widget build(BuildContext context) {
    int n = voteAvarage ~/ 2;
    List<Widget> widgets = List.generate(
        5,
        (index) => Icon(
              index < n ? MdiIcons.star : MdiIcons.starOutline,
              color: accentColor2,
              size: starSize,
            ));
    widgets.add(SizedBox(width: 3));
    widgets.add(Text(
      "$voteAvarage/10",
      style: whiteNumberFont.copyWith(
          color: color ?? Colors.white,
          fontWeight: FontWeight.w300,
          fontSize: fontSize),
    ));
    return Row(
      mainAxisAlignment: alignment,
      children: widgets,
    );
  }
}
